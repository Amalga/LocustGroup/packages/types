declare module "*.vue" {
  import Vue from 'vue'
  export default Vue
}

import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $localForage: any
  }
}

declare module '@nuxt/vue-app' {
  interface Context {
      $localForage: any
  }
  interface NuxtAppOptions {
      $localForage: any
  }
}

declare module '@nuxt/types' {
  interface Context {
      $localForage: any
  }
  interface NuxtAppOptions {
      $localForage: any
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    user: import('../definitions/user').AmalgaUserData
  }
}
