export interface AmalgaUtilities {
  fromHeaderOrQuerystring(request: import('express').Request): string | null
  logErrors(error: any, _request: import('express').Request, _response: import('express').Response, next: any): void
  errorHandler(error: any, _request: import('express').Request, response: import('express').Response, next: import('express').NextFunction): void
}
