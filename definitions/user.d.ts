import express from 'express'

export interface AmalgaRequest extends express.Request {
  user?: AmalgaUserData
}

export interface UserPlatforms {
  [platform: string]: any
}

export interface UserAchievements {
  progress?: number
}

export interface AmalgaUserData {
  isLoggedIn?: boolean
  user?: AmalgaUser | {}
}

export interface AmalgaUser {
  _id?: import('mongodb').ObjectId
  username?: string
  emails?: [
    {
      email: string
      primaryEmail?: boolean
      displayEmail?: boolean
    }
  ]
  password?: string
  passwordH?: string
  registrationDate?: string
  role?: [number, string],
  platforms?: {[key: string]: UserPlatforms},
  achievements?: UserAchievements
}

export interface AmalgaUsers {
  [user: string]: AmalgaUserData
}

export interface AmalgaRoles {
  Admin: [number, string]
  Guest: [number, string]
  User: [number, string]
}
