export interface IGame {
  id: number,
  name: string
}

export interface IGames {
  [title: string]: IGame
}
