import './declarations/modules'
import './declarations/namespaces'
import './extensions'


export { IGame, IGames } from './definitions/games'
export { AmalgaRequest, AmalgaUser, AmalgaUserData, AmalgaUsers, AmalgaRoles } from './definitions/user'
export { AmalgaUtilities } from './definitions/utilities'
