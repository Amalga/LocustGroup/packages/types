# Amalga Types
As such, treated like such.  

For now, that line above remains. *(and this one explaining that ...)*

Initially created for [project locust<sup>2</sup>](https://gitlab.com/Amalga/Locust/horde).

## Inclusion
Make sure to somehow<sup>1</sup> import the `index.d.ts` file from the root folder into your project  
*(most likely in your `tsconfig`)*

## Extensions `| extensions.d.ts`
This is for any `types` from 3rd party **modules**/**packages** to make them work. They may also not yet be defined or said `type` is not found in the [DefinitelyTyped](http://definitelytyped.org/) --> [Type Definition Library (TypeSearch)](https://microsoft.github.io/TypeSearch/) 

### Notes
<sup>**1 |**</sup> In different projects I have seen `types`, `typeRoots` and `includes` being used to get **Definitions** loaded.
editor.codeActionsOnSave instead with a source.fixAll.eslint member.  
<sup>**2 |**</sup> This was the original project that branched into what is kind of the [holder to them all](https://gitlab.com/Amalga/Locust/queen) -*a ¿monorepo‽*  

*The `gitlab` links from within will work at some point (working on getting repos flipped to `public`)* 
